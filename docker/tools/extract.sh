#!/usr/bin/env bash


if [ -d "/data/mmaps" ]; then
  echo Found mmaps dir. Skipping extraction run.
  exit 1
fi

cd /wow

rm -rf dbc maps vmaps Buildings mmaps

/trinity/out/bin/mapextractor

cp -r dbc maps /data

/trinity/out/bin/vmap4extractor
mkdir vmaps
/trinity/out/bin/vmap4assembler Buildings vmaps
cp -r vmaps /data

mkdir mmaps
/trinity/out/bin/mmaps_generator
cp -r mmaps /data

