#!/bin/bash

WOW_PATH="$@"


if [ -z "$WOW_PATH" ]; then
  echo ""
  echo ""
  echo ""

  echo Specify --wow-dir=/path/to/wow_dir for tools shit /!\

  echo ""
  echo ""
  echo ""
else
  echo WOW_PATH: $WOW_PATH
fi

cp ./src/server/worldserver/worldserver.conf.dist ./docker/config/worldserver.conf
cp ./src/server/authserver/authserver.conf.dist   ./docker/config/authserver.conf

sed -i 's/DataDir = "\."/DataDir = "\/data"/g' ./docker/config/worldserver.conf
sed -i 's/LoginDatabaseInfo *= "127\.0\.0\.1/LoginDatabaseInfo     = "db/g' ./docker/config/authserver.conf
sed -i 's/LoginDatabaseInfo     = "127\.0\.0\.1/LoginDatabaseInfo     = "db/g' ./docker/config/worldserver.conf
sed -i 's/WorldDatabaseInfo     = "127\.0\.0\.1/WorldDatabaseInfo     = "db/g' ./docker/config/worldserver.conf
sed -i 's/CharacterDatabaseInfo = "127\.0\.0\.1/CharacterDatabaseInfo = "db/g' ./docker/config/worldserver.conf

sed -i 's@source: \${WOW_PATH.*}@source: ${WOW_PATH:-'"$WOW_PATH"'}@g' ./docker-compose.yml


docker-compose up tools


docker-compose stop db
docker-compose rm -f db

echo Waiting...
( docker-compose up db & ) | grep -q "GENERATED"
echo Done waiting

PWD=`docker-compose logs db | grep GENERATED | sed -n "s/.*GENERATED ROOT PASSWORD: \(.*\)/\1/p"`
echo Found password: $PWD

echo Waiting for MySQL startup...
( docker-compose logs -f db & ) | grep -q "Starting MySQL"
echo Done.


winpty docker-compose exec db mysql --connect-expired-password -uroot -p${PWD} -e "ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';"
winpty docker-compose exec db mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON * . * TO 'root'@'%' IDENTIFIED BY 'root';"

winpty docker-compose exec db mysql -uroot -proot -e "source /trinity/sql/create/create_mysql.sql"
winpty docker-compose exec db mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON * . * TO 'trinity'@'%' IDENTIFIED BY 'trinity';"

echo Done with mysql init.


docker-compose up tools

docker-compose up

